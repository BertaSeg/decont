

for url in $(cat data/urls | sed 's:data/urls/::' | sort | uniq)
do
    bash scripts/downloadyes.sh $url data
done



bash scripts/downloadyes.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res



bash scripts/index.sh res/contaminants.fasta res/contaminants_idx



for sid in $(ls data/*.fastq.gz | cut -d "-" -f1 | sed 's:data/::' | sort | uniq)
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done



conda install cutadapt
mkdir -p log/cutadapt
mkdir -p out/trimmed

for sid in $(ls data/*.fastq.gz | cut -d "-" -f1 | sed 's:data/::' | sort | uniq) 
do
	cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sid}.trimmed.fastq.gz out/merged/${sid}.fastq.gz > log/cutadapt/${sid}.log
done



conda install star
for sid in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sed 's:out/merged/::' | sort | uniq) 
do
	mkdir -p out/star/$sid
	STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/${sid}.trimmed.fastq.gz --readFilesCommand zcat --outFileNamePrefix out/star/${sid}/
done



for sid in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sed 's:out/merged/::' | sort | uniq) 
do
	cat out/star/$sid/Log.final.out | grep -e "Uniquely mapped reads %" -e "% of reads mapped to multiple loci" -e "% of reads mapped to too many loci" >> log/pipeline.log
	cat log/cutadapt/$sid.log | grep -e "Reads with adapters" -e "Total basepairs" >> log/pipeline.log
done
