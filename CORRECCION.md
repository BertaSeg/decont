### downloadRG.sh

- [-1] El script no es genérico y reutilizable, sino que descarga los datos específicos del ejercicio
- [-1] El script depende de un error para no realizar dos descargas

### merge_fastqs.sh

- [-0.5] Se utilizan patrones específicos para dos archivos diferentes en lugar de sólo el sample id ($1/$sid*.fastq.gz)

### pipeline.sh

- [-1] El script no debería instalar paquetes conda, menos de forma interactiva
- [-0.5] El log final no especifica a qué muestra corresponden los datos
